# naerao
![Screenshot](./assets/screenshot.png)
Convert ACSM files to PDF/EPUBs

## Installation
The primary method of installation is through
[nix](https://github.com/NixOS/nix), but it should be possible to install it
without.

### With nix
If you have [nix](https://github.com/NixOS/nix) with
[flakes](https://nixos.wiki/wiki/Flakes) enabled, you can run the following
command to install naerao.
```shell
nix profile install gitlab:naerao/naerao
```
If you are on [NixOS](https://nixos.org/), you can add the flake to your system config.


### Without nix
*naerao* requires [libgourou](https://indefero.soutade.fr/p/libgourou/) and can
then be compiled and installed with cargo.
```shell
cargo install --git https://gitlab.com/naerao/naerao.git
```

## Usage
```
naerao <path_to_acsm_files...>
```

### Output location
You can change the output location with `-o` or `--output`. The `output`
argument takes a string which specifies the output location. The string can
contain templating arguments in curly-braces.

Valid template arguments are:
- `title`: Title of book
- `author`: Author of book
- `publisher`: Publisher of book
- `parent`: Parent directory
- `ext`: File type extension

**Example**
```shell
naerao -o "{parent}/{title}.{ext}" <path_to_acsm_file>
```
