mod arguments;
mod formatting;

use std::path::Path;
use anyhow::{Result, anyhow};
use arguments::Arguments;
use colored::{Colorize, ColoredString};
use acsm_rs::Acsm;
use libgourou_rs::Gourou;

fn main() -> Result<()> {
    let args = arguments::parse()?;

    println!("Creating client");
    let libgourou_dir = dirs::state_dir()
        .ok_or_else(|| anyhow!("Could not find location for state files"))?
        .join("naerao")
        .join("libgourou");
    let mut gourou = Gourou::new(&libgourou_dir);

    println!("Signing in");
    gourou.sign_in_anonymous();

    for acsm_file in &args.acsm_files {
        match download_book(&mut gourou, &acsm_file, &args) {
            Ok(()) => (),
            Err(err) => println!("{err}"),
        }
    }
    return Ok(());
}

/// Create decorated book title from acsm metadata or use default
fn create_decorated_book_title(acsm_metadata: &Acsm) -> ColoredString {
    acsm_metadata.title
        .as_ref()
        .map(|title| title.clone().yellow())
        .unwrap_or("book".white())
}

/// Download book from acsm file
fn download_book(gourou: &mut Gourou, acsm_file: &Path, args: &Arguments) -> Result<()> {
    let acsm_metadata = Acsm::from_file(acsm_file)?;
    let title = create_decorated_book_title(&acsm_metadata);
    println!("Downloading {title} from {}", acsm_file.to_string_lossy().cyan());

    let drm_file = acsm_file.with_extension("drm");
    let file_type = gourou.download(&acsm_file, &drm_file);

    println!("Removing DRM");
    let output = formatting::format_output_path(&args.output_template, &acsm_metadata, &file_type, &acsm_file)?;
    gourou.remove_drm(&drm_file, &output, &file_type);
    println!("Saved book at {}", output.to_string_lossy().cyan());

    std::fs::remove_file(acsm_file)?;

    return Ok(());
}
