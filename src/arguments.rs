use std::path::PathBuf;

use anyhow::Result;
use clap::Parser;

#[derive(clap::Parser, Debug)]
#[command(author, version, about)]
pub struct Arguments {
    /// Path to acsm files
    pub acsm_files: Vec<PathBuf>,
    /// Output location of decrypted files
    #[arg(short, long="output", default_value="{parent}/{title}.{ext}")]
    pub output_template: String,
}

/// Parse cli arguments
pub fn parse() -> Result<Arguments> {
    let arguments = Arguments::parse();
    return Ok(arguments);
}
