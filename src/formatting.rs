use std::{collections::HashMap, path::{Path, PathBuf}, str::FromStr};
use acsm_rs::Acsm;
use libgourou_rs::FileType;
use rt_format::{Format, FormatArgument, ParsedFormat, Specifier};
use anyhow::{Result, anyhow, Context};


#[derive(Debug)]
enum Value<'a> {
    AcsmValue(&'a Option<String>),
    Path(PathBuf),
    Static(&'static str),
}

impl<'a> From<&'a Option<String>> for Value<'a> {
    fn from(value: &'a Option<String>) -> Self {
        Self::AcsmValue(value)
    }
}

impl<'a> From<Option<&Path>> for Value<'a> {
    fn from(value: Option<&Path>) -> Self {
        match value {
            Some(path) => Value::Path(path.to_path_buf()),
            None => Value::Static("UNKNOWN"),
        }
    }
}

impl<'a> FormatArgument for Value<'a> {
    fn supports_format(&self, specifier: &Specifier) -> bool {
        specifier.format == Format::Display
    }

    fn fmt_display(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Value::AcsmValue(x) => {
                let value = match x {
                    Some(x) => x,
                    None => "UNKNOWN",
                };
                std::fmt::Display::fmt(&value, f)
            },
            Value::Path(path) => {
                std::fmt::Display::fmt(&path.to_string_lossy(), f)
            },
            Value::Static(value) => std::fmt::Display::fmt(value, f)
        }
    }

    fn fmt_debug(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        std::fmt::Debug::fmt(self, f)
    }

    fn fmt_octal(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        Err(std::fmt::Error)
    }

    fn fmt_lower_hex(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        Err(std::fmt::Error)
    }

    fn fmt_upper_hex(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        Err(std::fmt::Error)
    }

    fn fmt_binary(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        Err(std::fmt::Error)
    }

    fn fmt_lower_exp(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        Err(std::fmt::Error)
    }

    fn fmt_upper_exp(&self, _f: &mut std::fmt::Formatter) -> std::fmt::Result {
        Err(std::fmt::Error)
    }
}

type OutputOptions<'a> = HashMap<&'static str, Value<'a>>;

fn book_options<'a>(acsm: &'a Acsm, file_type: &FileType, path: &Path) -> OutputOptions<'a> {
    HashMap::from([
        ("title", (&acsm.title).into()),
        ("author", (&acsm.creator).into()),
        ("publisher", (&acsm.publisher).into()),
        ("parent", path.parent().into()),
        ("ext", Value::Static(file_type.extension())),
    ])
}

/// Create string format from book metadata
pub fn format_book(template: &str, acsm: &Acsm, file_type: &FileType, path: &Path) -> Result<String> {
    let named_options = book_options(acsm, file_type, path);
    let args = ParsedFormat::parse(template, &[], &named_options)
        .map_err(|_| anyhow!("Failed to create format string from book metadata"))?;
    return Ok(format!("{}", args));

}

/// Create path formatted from book metadata
pub fn format_output_path(template: &str, acsm: &Acsm, file_type: &FileType, path: &Path) -> Result<PathBuf> {
    let format_string = format_book(template, acsm, file_type, path)?;
    PathBuf::from_str(&format_string)
        .with_context(|| "Failed to create output path")
}
