Output assets/demo.mp4

Set CursorBlink false
Set Framerate 25
Set PlaybackSpeed 2.0

Set Margin 20
Set MarginFill "#cea210"
Set BorderRadius 10

Set FontSize 30
Set FontFamily "InconsolataGo Nerd Font"

Set Width 1920
Set Height 720

Set Shell bash

Hide
Type "PS1='> '"
Enter
Type "clear"
Enter
Show

Type@100ms "naerao ~/Downloads/URLLink.acsm"

Sleep 200ms

Enter

Sleep 30s
Screenshot assets/screenshot.png
