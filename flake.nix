{
  description = "Convert ACSM files to PDFs/EPUBs";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }:
  flake-utils.lib.eachDefaultSystem(system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {
    packages.default = pkgs.rustPlatform.buildRustPackage {
      pname = "naerao";
      version = "0.1.0";

      src = ./.;
      cargoLock = {
        lockFile = ./Cargo.lock;
        outputHashes = {
          "libgourou-rs-0.1.0" = "sha256-7ch6HJ8rwPXg5eHDf8msbPIw3I+1RVgolRcBSR9b3gw=";
          "acsm-rs-0.1.0" = "sha256-Jh7q9yIGXhlEjM7NIJOLLBUkML6AqP/mZ8+BJ1OV/hA=";
        };
      };

      buildInputs = with pkgs; [
        clang
        libgourou
        pugixml
        openssl.dev
        curl.dev
        libzip.dev
        zlib.dev
      ];

    };
    devShells.default = pkgs.mkShell {
      buildInputs = with pkgs; [
        rustc
        cargo

        clang
        libgourou
        pugixml
        openssl.dev
        curl.dev
        libzip.dev
        zlib.dev
      ];
    };
  });
}
